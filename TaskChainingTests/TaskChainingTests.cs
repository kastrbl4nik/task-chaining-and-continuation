using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using TaskChainingTasks;

namespace TaskChainingTests
{
    public class TaskChainingTest
    {
        [SetUp]
        public void Setup()
        {
            TaskChaining.Seed = 228;
        }

        [Test]
        public void CreateArray()
        {
            int[] expected = new int[] { 8, 7, 2, 3, 2, 8, 1, 8, 2, 4 };
            int[] actual = TaskChaining.CreateArray();
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MultiplyArray()
        {
            int[] sourceArray = new int[] { 1, 2, 3, 4, 5 };
            int[] expected = new int[] { 8, 16, 24, 32, 40 };
            int[] actual = TaskChaining.MultiplyArray(sourceArray);
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void SortArray()
        {
            int[] sourceArray = new int[] { 5, 3, 2, 1, 4 };
            int[] expected = new int[] { 1, 2, 3, 4, 5 };
            int[] actual = TaskChaining.SortArray(sourceArray);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MultiplyArray_ThrowsArgumentException_WhenArrayIsEmpty()
        {
            Assert.Throws<ArgumentException>(() => TaskChaining.MultiplyArray(Array.Empty<int>()));
        }

        [Test]
        public void SortArray_ThrowsArgumentException_WhenArrayIsEmpty()
        {
            Assert.Throws<ArgumentException>(() => TaskChaining.MultiplyArray(Array.Empty<int>()));
        }
    }
}