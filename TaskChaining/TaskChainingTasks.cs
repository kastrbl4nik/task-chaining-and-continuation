﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskChainingTasks
{
    public static class TaskChaining
    {
        public static int Seed { get; set; }

        public static int[] CreateArray()
        {
            Random random = new(Seed);
            int[] result = new int[10];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = random.Next(1, 10);
            }
            return result;
        }

        public static int[] MultiplyArray(int[] sourceArray)
        {
            if (sourceArray.Length < 1)
            {
                throw new ArgumentException("Array cannot be empty", nameof(sourceArray));
            }

            Random random = new(Seed);
            int[] result = new int[sourceArray.Length];
            int randomNumber = random.Next(1, 10);
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = sourceArray[i] * randomNumber;
            }
            return result;
        }

        public static int[] SortArray(int[] sourceArray)
        {
            if (sourceArray.Length < 1)
            {
                throw new ArgumentException("Array cannot be empty", nameof(sourceArray));
            }

            int[] result = new int[sourceArray.Length];
            Array.Copy(sourceArray, result, sourceArray.Length);
            Array.Sort(result);
            return result;
        }

        public static void ChainTasks()
        {
            var task1 = Task.Run(() => CreateArray());
            var task2 = task1.ContinueWith(t => MultiplyArray(t.Result));
            var task3 = task2.ContinueWith(t => SortArray(t.Result));
            var task4 = task3.ContinueWith(t => t.Result.Average());

            task4.Wait();

            Console.WriteLine("Original Array: " + String.Join(", ", task1.Result));
            Console.WriteLine("Multiplied Array: " + String.Join(", ", task2.Result));
            Console.WriteLine("Sorted Array: " + String.Join(", ", task3.Result));
            Console.WriteLine("Average: " + task4.Result);
        }
    }
}
